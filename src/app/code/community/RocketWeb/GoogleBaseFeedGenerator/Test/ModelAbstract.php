<?php

class RocketWeb_GoogleBaseFeedGenerator_Test_ModelAbstract extends EcomDev_PHPUnit_Test_Case
{
    protected $className = '';
    public $model = null;

    protected function setUp($loadModel = true)
    {
        parent::setUp();
        if ($loadModel) {
            $this->model = Mage::getModel($this->className);
        }
    }

    protected function tearDown()
    {
        unset($this->model);
        parent::tearDown();
    }

    protected function debug($msg)
    {
        fwrite(STDOUT, var_export($msg,1) . "\n");
    }

    /**
     * Tests that the model was setup correctly
     *
     * @test
     */
    public function testSetup()
    {
        $this->assertTrue(is_object($this->model), sprintf('Model "%s" was not setup correctly!', $this->className));
    }


    /**********************/
    /*** HELPER METHODS ***/
    /**********************/

    /**
     * @param array $array
     * @param string $indent
     * @param array $placeholders
     * @return string
     */
    public function toYaml($array = array(), $indent = '', $placeholders = array())
    {
        $output = "\n";
        $keyString = isset($placeholders['key']) ? $placeholders['key'] : false;
        $valueString = isset($placeholders['value']) ? $placeholders['value'] : false;
        $staticString = isset($placeholders['static']) ? $placeholders['static'] : false;
        foreach ($array as $key => $value) {
            if ($keyString !== false && $valueString !== false) {
                $output .= $indent . '- ' . ($staticString !== false ? $staticString : '') . "\n";
                $output .= $indent . '  ' . $keyString . ': ' . $key . "\n";
                $output .= $indent . '  ' . $valueString . ': ' . $value . "\n";
            } else {
                $output .= $indent . $key . ':';
                if (is_array($value)) {
                    $output .= $this->toYaml($value, $indent . '  ');
                } else {
                    $output .= $value . "\n";
                }
            }
        }
        return $output;
    }

    /**
     * Replace tax/calculation model with mock model
     *
     * @param int $percent
     */
    protected function setTaxCalculationMock($percent = 10)
    {
        /**
         * Setting up $percent tax on the price for testing
         */
        $mockModel = $this->getModelMock('tax/calculation', array('getRateRequest', 'getAppliedRates'));
        $mockModel->expects($this->any())
            ->method('getAppliedRates')
            ->will($this->returnValue(
                array(
                    array('percent' => $percent),
                    array('percent' => 0)
                )
            ));

        $mockModel->expects($this->any())
            ->method('getRateRequest')
            ->will($this->returnCallback(
                array($this, 'mockMethod')
            ));

        $this->replaceByMock('singleton', 'tax/calculation', $mockModel);
    }

    protected function setProductResourceMock()
    {
        $mockModel = $this->getModelMock('catalog/resource_product', array('refreshEnabledIndex'));
        $mockModel->expects($this->any())
            ->method('refreshEnabledIndex')
            ->will($this->returnSelf());

        $this->replaceByMock('model', 'catalog/resource_product', $mockModel);
    }

    /**
     * Mock method
     *
     * @param $shippingAddress
     * @param $billingAddress
     * @param $ctc
     * @param $store
     * @return Varien_Object
     */
    public function mockMethod($shippingAddress, $billingAddress, $ctc, $store)
    {
        return new Varien_Object();
    }
}