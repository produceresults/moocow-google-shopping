<?php

class RocketWeb_GoogleBaseFeedGenerator_Test_GeneratorAbstract extends RocketWeb_GoogleBaseFeedGenerator_Test_ModelAbstract
{
    protected $className = 'googlebasefeedgenerator/generator';
    /** @var RocketWeb_GoogleBaseFeedGenerator_Model_Generator */
    public $model = null;

    /**
     * We disable model loading since it needs a feed from fixtures
     */
    protected function setUp()
    {
        parent::setUp(false);
    }

    protected function setModel($resetFeedFiles = false, $feedId = 1)
    {
        $feed = Mage::getModel('googlebasefeedgenerator/feed')->load($feedId);
        $feed->setData('feed_filename', 'phpunit_feed_file');
        $feed->setData('log_filename', 'phpunit_feed_log');

        $this->unsetRegister($feed);
        if ($resetFeedFiles) {
            $this->tearFeedFiles($feed);
        }
        // Msrp enabled
        $feed->getStore()->setConfig('sales/msrp/enabled', '2');

        $this->model = Mage::helper('googlebasefeedgenerator')->getGenerator($feed);
    }

    protected function unsetRegister($feed)
    {
        $keys = array(
            'googlebasefeedgenerator/entity_type/catalog_product',
            '_singleton/googlebasefeedgenerator/tools',
            '_singleton/googlebasefeedgenerator/generator_feed_' . $feed->getId()
        );
        foreach ($keys as $key) {
            Mage::unregister($key);
        }
    }

    /**
     * @param RocketWeb_GoogleBaseFeedGenerator_Model_Feed $feed
     */
    protected function tearFeedFiles($feed)
    {
        $logFile = Mage::getBaseDir('var') . DS . 'log'. DS. $feed->getLogFile();
        $feedFile = $this->getFeedFile($feed);
        @unlink($logFile);
        @unlink($feedFile);
    }

    protected function getFeedFile($feed)
    {
        return Mage::getBaseDir() . DS . $feed->getConfig('general_feed_dir') . DS . $feed->getFeedFile();
    }

    protected function prepareResult($text)
    {
        $domain = 'http://local.gsf2.dev/';
        // Replacing index.php/
        $newDomain = str_replace('index.php/', '', Mage::getBaseUrl());
        return str_replace($domain, $newDomain, $text);
    }

    /**
     * Tests that the model was setup correctly
     *
     * @test
     */
    public function testSetup()
    {
        // Dummy test for Product tests
        $this->assertEquals(true, true);
    }

    /**
     * This must be called from the child object (aka RocketWeb_GoogleBaseFeedGenerator_Test_Model_SimpleProduct)
     */
    public function generateFeedConfig($feedId = 1)
    {
        $feed = Mage::getModel('googlebasefeedgenerator/feed')->load($feedId);
        $config = $feed->getConfig();

        $configModel = Mage::getModel('googlebasefeedgenerator/config');
        $newConfig = array();
        foreach ($config as $key => $value) {
            $configModel->setData(array(
                    'path' => $key,
                    'value' => $value
            ));
            //We change the value to the DB format
            $configModel->_beforeSave();
            $newConfig[$key] = $configModel->getValue();
        }

        $output = $this->toYaml($newConfig, '  ', array('key' => 'path', 'value' => 'value', 'static' => 'feed_id: '.$feedId));
        $this->debug($output);
    }
}