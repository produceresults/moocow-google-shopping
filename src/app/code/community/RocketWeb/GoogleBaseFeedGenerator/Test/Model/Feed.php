<?php


class RocketWeb_GoogleBaseFeedGenerator_Test_Model_Feed extends RocketWeb_GoogleBaseFeedGenerator_Test_ModelAbstract
{
    protected $className = 'googlebasefeedgenerator/feed';
    /** @var RocketWeb_GoogleBaseFeedGenerator_Model_Feed */
    public $model = null;

    /**
     * Test if default config is ok and loading from DB only works
     * TODO: Need to change this to dataProviders and expectations, so each provider config can be added easily
     *
     * @test
     * @loadFixture feed
     * @loadFixture config
     */
    public function getConfig()
    {
        $this->model->load(0);
        $configXml = $this->model->getConfig();
        $this->model->load(1);
        $configDb = $this->model->getConfig(null, false);
        $this->assertEquals($configDb, $configXml, sprintf("Default config data (XML) has been changed!"));

    }

    /**
     * Checks that feed url gets generated correctly
     *
     * @test
     * @loadFixture feed
     * @loadFixture config
     * @loadExpectation
     */
    public function getFeedUrl()
    {
        $baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $expectedUrl = $baseUrl . $this->expected('url')->getFeedUrl();

        $this->model->load(1);
        $feedUrl = $this->model->getFeedUrl();

        $this->assertEquals($expectedUrl, $feedUrl, 'Feed url has been changed!');
    }
}