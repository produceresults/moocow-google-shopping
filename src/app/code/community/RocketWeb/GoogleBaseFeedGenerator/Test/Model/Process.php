<?php


class RocketWeb_GoogleBaseFeedGenerator_Test_Model_Process extends RocketWeb_GoogleBaseFeedGenerator_Test_ModelAbstract
{
    protected $className = 'googlebasefeedgenerator/process';

    /**
     * Testing that the return of initialize() returns the same object
     *
     * @test
     * @loadFixture
     */
    public function testInitialize()
    {
        $model = $this->model->initialize();
        $this->assertTrue(
            is_object($model),
            sprintf('%s::%s returned non-object', $this->className, __METHOD__)
        );

        $this->assertEquals(
            get_class($this->model),
            get_class($model),
            sprintf('%s::%s returned wrong object!', $this->className, __METHOD__)
        );
    }

    /**
     * Testing that status gets changed to processed
     *
     * @test
     */
    public function testProcess()
    {
        $this->assertEquals(
            $this->model->process()->getStatus(),
            RocketWeb_GoogleBaseFeedGenerator_Model_Process::STATUS_PROCESSED,
            sprintf('%s::%s returned wrong status!', $this->className, __METHOD__)
        );
    }

}