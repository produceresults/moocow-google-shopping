<?php


class RocketWeb_GoogleBaseFeedGenerator_Test_Model_ConfigurableProduct extends RocketWeb_GoogleBaseFeedGenerator_Test_GeneratorAbstract
{
    /**
     * @test
     * @loadFixture testRunFeeds
     * @loadFixture testRunConfigs
     * @loadFixture configurableProducts
     * @loadFixture configurableProductsEav
     * @loadExpectation
     * @dataProvider dataProvider
     */
    public function configurableProductsFeeds($feedId, $options = array())
    {
        $this->setModel(true, $feedId);
        if (isset($options['tax'])) {
            $this->setTaxCalculationMock($options['tax']);
        }
        $this->setProductResourceMock();

        $this->model->run();

        $feed = $this->model->getFeed();
        $feedText = file_get_contents($this->getFeedFile($feed));

        if (isset($options['debug'])) {
            $this->debug($feedText);
            return;
        }
        $expectedFeed = $this->expected('feed_%s_%s', $feedId, implode('_', array_values($options)))->getResult();
        $expectedFeed = $this->prepareResult($expectedFeed);
        $this->assertEquals($expectedFeed, explode("\n", $feedText), sprintf('Feed "%s" output is wrong!', $feed->getName()));
    }

    /**
     * test
     * loadFixture testRunFeeds
     *
    public function generateFeedConfig()
    {
        $feedId = 3;
        parent::generateFeedConfig($feedId);
    }*/
}