<?php


class RocketWeb_GoogleBaseFeedGenerator_Test_Model_SimpleProduct extends RocketWeb_GoogleBaseFeedGenerator_Test_GeneratorAbstract
{
    /**
     * @test
     * @loadFixture testRunFeeds
     * @loadFixture testRunConfigs
     * @loadFixture simpleProducts
     * @loadFixture simpleProductsEav
     * @loadExpectation
     * @dataProvider dataProvider
     */
    public function simpleProductsFeeds($feedId, $options = array())
    {
        $this->setModel(true, $feedId);
        if (isset($options['tax'])) {
            $this->setTaxCalculationMock($options['tax']);
        }

        $this->model->run();

        $feed = $this->model->getFeed();
        $feedText = file_get_contents($this->getFeedFile($feed));

        if (isset($options['debug'])) {
            $this->debug($feedText);
            return;
        }
        $expectedFeed = $this->expected('feed_%s_%s', $feedId, implode('_', array_values($options)))->getResult();
        $expectedFeed = $this->prepareResult($expectedFeed);
        $feedText = explode("\n", $feedText);
        $this->assertEquals($expectedFeed, $feedText, sprintf('Feed "%s" output is wrong!', $feed->getName()));
    }

    /**
     * test
     * loadFixture testRunFeeds
     *
    public function generateFeedConfig()
    {
        $feedId = 3;
        parent::generateFeedConfig($feedId);
    }*/
}