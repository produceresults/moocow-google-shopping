<?php


class RocketWeb_GoogleBaseFeedGenerator_Test_Model_Generator extends RocketWeb_GoogleBaseFeedGenerator_Test_GeneratorAbstract
{
    /**
     * Tests that the model was setup correctly
     *
     * @test
     * @loadFixture feed
     */
    public function testSetup()
    {
        $this->setModel();
        $this->assertTrue(is_object($this->model), sprintf('Model "%s" was not setup correctly!', $this->className));
    }

    /**
     * @test
     * @loadFixture feed
     * @loadExpectation
     * @dataProvider dataProvider
     */
    public function getColumnsMap($feedId)
    {
        $expected = $this->expected('map_%s', $feedId)->getData();

        $this->setModel(false, $feedId);
        $map = $this->model->getColumnsMap();

        $this->assertEquals($expected, $map, "Columns map was changed!");
    }

    /**
     * @test
     * @loadFixture feed
     * @loadFixture config
     * @loadFixture simpleProducts
     * @loadExpectation
     */
    public function getCollection()
    {
        $this->setModel();
        $collection = $this->model->getCollection();
        foreach ($collection as $product) {
            $expected = $this->expected("product_%s", $product->getId())->getData();
            $array = $product->toArray();
            $this->assertEquals($expected, $array, "Product collection missmatch!");
        }
    }
}